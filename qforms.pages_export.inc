<?php

/**
 * @file
 * Export menu callbacks for the qforms module.
 */

/**
 * Page for exporting submission results for given node.
 */
function qforms_results_submissions_export($node) {
  $exported_results = qforms_results_submissions_export_base($node);
  if ($exported_results==='') {    
    return '';
  }
  else {
    header('Content-Type: "application/octet-stream; charset=utf-8"');
    header('Content-Disposition: attachment; filename="' . $exported_results['file_name'] . '"');
    print $exported_results['file_content'];
    exit();
  }
}

/**
 * Callback for submissions results export to csv file.
 *
 * This function is also used for testing.
 *
 * @param $node
 * @return
 *   array that contain file name and file content
 */
function qforms_results_submissions_export_base($node) {
  $entity_id = $node->nid;
  $output = '';

  // Create select statement for qforms submissions.
  $select = db_select('qforms_submission', 's');

  // Show submissions just for this entity id.
  $select->condition('s.nid', $entity_id);

  // Join the users table, so we can get the entry creator's username.
  $select->join('users', 'u', 's.uid = u.uid');
  $select->join('qforms_definition', 'd', 's.nid = d.nid');
  $select->join('node', 'n', 'n.nid = s.nid');

  // Select these specific fields for the output.
  $select->addField('s', 'sid');
  $select->addField('s', 'time');
  $select->addField('s', 'data');
  $select->addField('d', 'form_definition');
  $select->addField('u', 'name', 'username');
  $select->addField('n', 'title', 'node_title');

  $rows = $select->execute()->fetchAll(PDO::FETCH_ASSOC);

  if (!empty($rows)) {
    $first_row = current($rows);
    $file_name = check_plain($first_row['node_title']) . ' - Submission results.csv';

    $header = t('"Id", "Time", "Submited by"');

    // $form_structures contain field titles
    $form_structures = unserialize($first_row['form_definition']);
    reset($form_structures);
    foreach ($form_structures as $form_structure) {
       if ($form_structure['type'] == 'scale') {
        $fsv = $form_structure['values'];
        reset($fsv);
        while (next($fsv) != FALSE) {
          $scale_item = current($fsv);
          $scale_item_title = current($scale_item);
          $header .= ',"' . check_plain($form_structure['title']) . ' - ' . current($scale_item_title) . '"';
        }
      }
      else {
        $header .= ',"' . check_plain($form_structure['title']) . '"';
      }
    }

    foreach ($rows as &$row) {
      $submited_values = '';

      // $datas contain entered data from survay's fields
      $datas = unserialize($row['data']);
      foreach ($datas as $data) {
        if (is_string($data)) {
          $submited_values .= ',"' . check_plain($data) . '"';
        }
        elseif (is_array($data)) {
          // for check box field there can be multiple values
          foreach ($data as $key => $value) {
            if ($key === $value) {
              $submited_values .= ',"' . check_plain($value) . '"';
            }
          }
        }
      }
      $output .= "\n" . '"' . $row['sid'] . '", "' . format_date($row['time']) . '", "' . check_plain($row['username']) . '"' . $submited_values;
    }

    return array('file_name' => $file_name, 'file_content' => ($header . $output));
  }
  else {
    drupal_set_message(t('No submission results.'));
    return '';
  }

}
