<?php

/**
 * @file
 * Functions for transforming qforms form elements definitions to real Drupal
 * form elements.
 */

/**
 * Returns textfield Drupal form element.
 */
function qforms_fapi_textfield($element, array $params = array()) {
  $felement = _qforms_fapi_text_element($element, $params);
  if (isset($element['size']) && $element['size'] != '0') {
    $felement['#size'] = check_plain($element['size']);
  }
  $felement['#type'] = 'textfield';

  return $felement;
}

/**
 * Returns textarea Drupal form element.
 */
function qforms_fapi_textarea($element, array $params = array()) {
  $felement = _qforms_fapi_text_element($element, $params);
  if (isset($element['size']) && $element['size'] != '0') {
    $felement['#size'] = check_plain($element['size']);
  }
  $felement['#type'] = 'textarea';

  return $felement;
}

/**
 * Returns select Drupal form element.
 */
function qforms_fapi_select($element, array $params = array()) {
  $felement = _qforms_fapi_options_element($element, $params);
  $felement['#type'] = 'select';
  $felement['#multiple'] = ($element['multiple'] == 1) ? TRUE : FALSE;

  if ($element['other']) {
    $felement['#key'] = $element['key'];
    $felement['#theme_wrappers'] = array('qforms_select', 'form_element');
  }
  return $felement;
}

/**
 * Returns select Drupal form element.
 */
function qforms_fapi_checkboxes($element, array $params = array()) {
  $felement = _qforms_fapi_options_element($element, $params);
  $felement['#type'] = 'checkboxes';

  return $felement;
}

// value from other textfield must be unset since its value isn't an option in parent element
function qforms_fapi_options_after_build($form_element, &$form_state) {
  // If we are in submit.
  if (isset($form_state['input']['op'])) { // @TODO - check for better submit condition?
    if (is_array($form_element['#value'])) {
      $other_value = array_diff($form_element['#value'], $form_element['#options']);
      if (is_array($form_element['#value']) && isset($form_element['#value'][key($other_value)])) {
        unset($form_element['#value'][key($other_value)]);
      }
    }
  }

  return $form_element;
}
/**
 * Returns select Drupal form element.
 */
function qforms_fapi_radios($element, array $params = array()) {
  $felement = _qforms_fapi_options_element($element, $params, TRUE);
  $felement['#type'] = 'radios';

  return $felement;
}

/************************/
/*** Helper functions ***/
/************************/

/**
 * Returns generic options input element.
 */
function _qforms_fapi_options_element($element, $params, $radios = FALSE) {
  $options = array(
    '#title' => check_plain($element['title']),
    '#description' => check_plain($element['description']),
    '#required' => $element['required'] == 'true' ? TRUE : FALSE,
  );

  foreach ($element as $key => $value) {
    if (strpos($key, '_option_value')) {
      $form_value = check_plain($value);
      $options['#options'][$form_value] = $form_value;

      // Lets find default_value key.
      $default_key = str_replace('_option_value', '_default', $key);
      
      if (isset($params['default_value'])) { // We have stored default values.
        // Find correct value.
        if (is_array($params['default_value'])) { // multiselect and checkboxes
          $default_values = current($params['default_value']);
          next($params['default_value']);
        }
        else { // radios and single select
          $default_values = $params['default_value'];
        }

        if ($radios) {
          $options['#default_value'] = check_plain($default_values);
        }
        else {
          $options['#default_value'][] = check_plain($default_values);
        }
      }
      elseif ($element[$default_key] == TRUE) { // Take default values from form definition.
        if ($radios) {
          $options['#default_value'] = $form_value;
        }
        else {
          $options['#default_value'][] = $form_value;
        }
      }
    }

    if (isset($params['readonly'])) {
      $options['#attributes']['readonly'] = 'readonly';
    }

    if ($element['other']) {
      $options['#after_build'][] = 'qforms_fapi_options_after_build';
      $options[$element['key'] . '_other'] = array(
        '#type' => 'textfield',
        '#title' => t('Other'),
        '#size' => 60,
        '#maxlength' => 128,
        '#weight' => 100,
        '#required' => FALSE,
        '#default_value' => isset($params['default_value_other']) ? check_plain($params['default_value_other']) :
          (isset($params['default_value'][($element['key'] . '_other')]) ? check_plain($params['default_value'][($element['key'] . '_other')]) : ''),
      );
    }
  }
  return $options;
}

/**
 * Returns generic text input element.
 */
function _qforms_fapi_text_element($element, $params) {
  $felement = array(
    '#title' => check_plain($element['title']),
    '#description' => check_plain($element['description']),
    '#required' => $element['required'] == 'true' ? TRUE : FALSE,
  );

  if (isset($params['readonly'])) {
    $felement['#attributes']['readonly'] = 'readonly';
  }

  if (isset($params['default_value'])) {
    $felement['#default_value'] = $params['default_value']; // No need for check_plain because theme function will do this.
  }

  return $felement;
}
