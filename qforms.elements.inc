<?php

/**
 * @file
 * Holds functions for qforms elements definitions for form builder.
 */

function qforms_elements_textfield($element_data) {
  $element_data['element_title'] = t('Text field');
  $element = qforms_elements_default($element_data);
  qforms_elements_add_size($element_data, $element);
  return $element;
}

function qforms_elements_textarea($element_data) {
  $element_data['element_title'] = t('Paragraph field');
  $element = qforms_elements_default($element_data);
  qforms_elements_add_size($element_data, $element);
  return $element;
}

function qforms_elements_checkboxes($element_data) {
  $element_data['element_title'] = t('Checkbox group');
  $element = qforms_elements_default($element_data);

  $element_data['values'] = (isset($element_data['values']) ? $element_data['values'] : array(''));
  qforms_elements_add_options($element, $element_data);

  $element[$element_data['id'] . '_data'][$element_data['id'] . '_other'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add other'),
    '#title_display' => 'before',
    '#default_value' =>  (isset($element_data['other']) ? $element_data['other'] : false),
  );

  return $element;
}

function qforms_elements_select($element_data) {
  $element_data['element_title'] = t('Select list');
  $element = qforms_elements_default($element_data);


  $element_data['values'] = (isset($element_data['values']) ? $element_data['values'] : array(''));
  qforms_elements_add_options($element, $element_data);
  $element[$element_data['id'] . '_data'] += array(
    $element_data['id'] . '_multiple' => array(
      '#type' => 'checkbox',
      '#title' => t('Allow multiple selection'),
      '#title_display' => 'before',
      '#default_value' => (isset($element_data['multiple']) ? $element_data['multiple'] : false),
    )
  );
  $element[$element_data['id'] . '_data'][$element_data['id'] . '_other'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add other'),
    '#title_display' => 'before',
    '#default_value' =>  (isset($element_data['other']) ? $element_data['other'] : false),
  );

  return $element;
}

function qforms_elements_radios($element_data) {
  $element_data['element_title'] = t('Radio group');
  $element = qforms_elements_default($element_data);

  $element_data['values'] = (isset($element_data['values']) ? $element_data['values'] : array(''));
  qforms_elements_add_options($element, $element_data);

  $element[$element_data['id'] . '_data'][$element_data['id'] . '_other'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add other'),
    '#title_display' => 'before',
    '#default_value' =>  (isset($element_data['other']) ? $element_data['other'] : false),
  );

  return $element;
}

/**
 * Prepare element with options for saving. We will remove all buttons values
 * from element.
 */
function qforms_elements_options_prepare_for_saving(&$element) {
  // Remove delete option buttons values.
  foreach ($element as $key => $value) {
    if (strpos($key, '_delete_option') !== FALSE) {
      unset($element[$key]);
    }
  }

  // Remove add option button value.
  unset($element['options_add_option']);
}

/************************/
/*** Helper functions ***/
/************************/

function qforms_elements_add_options(&$element, $element_data) {
  $options = array(
    '#prefix' => '<div class="qforms-element-options form-item"><label>' . t('Select options') . '</label>',
    '#suffix' => '</div>',
    '#qforms-options-id' => 0,
  );

  // If we are creating a new options element then we need to add one empty option.
  if (!isset($element_data['other'])) {
    $element_data['options_0_option_value'] = '';
  }

  foreach ($element_data as $key => $value) {
    if (strpos($key, '_option_value')) {

      // Lets check is this option checked to be default.
      $default_key = str_replace('_option_value', '_default', $key);
      $checked = $element_data[$default_key] == TRUE ? TRUE : FALSE;

      qforms_elements_add_option($element_data['id'], $value, $options, $checked);
    }
  }

  $options[$element_data['id'] . '_options_add_option'] = array(
    '#type' => 'button',
    '#name' => $element_data['id'] . '_add_option', // This needs to be unique so ajax post can work with multiple buttons.
    '#value' => '',
    '#ajax' => array(
      'callback' => 'qforms_add_form_element_option_callback',
      'qforms_callback' => 'qforms_add_form_element_option',
      'wrapper' => str_replace('_', '-', 'edit_' . $element_data['id'] . '_options_add_option'),
      'effect' => 'fade',
      'method' => 'before',
      'qforms_parent' => $element_data['id'], // We need this reference to parent qforms element.
    ),
    '#attributes' => array('class' => array('qforms-button', 'qforms-action-add-option'), 'title' => t('Add option')),
  );

  $element[$element_data['id'] . '_data'][$element_data['id'] . '_options'] = $options;
}

function qforms_elements_add_option($parent_id, $value, &$options, $checked = FALSE) {
  // @TODO - (1) we maybe have a problem here when saving/editing options definitions
  // on the same node - option elements id can change because we are always
  // calculating $options['#qforms-options-id'] from the start. Maybe it is
  // better to store $options['#qforms-options-id'] also in db for form definition
  // and reuse it each time - this way options ids will be unique.
  ++$options['#qforms-options-id'];
  $okey = $parent_id . '_options' . '_' . $options['#qforms-options-id'];
  $options[$okey] = array(
    '#prefix' => '<div id="' . $okey . '" class="qforms-element-option ' . $okey . '">',
    '#suffix' => '</div>',
    $okey . '_default' => array(
      '#type' => 'checkbox',
      '#default_value' => $checked? 1 : 0,
    ),
    $okey . '_option_value' => array(
      '#type' => 'textfield',
      '#size' => '20',
      '#default_value' => isset($value) ? check_plain($value) : '',
    ),
    $okey . '_delete_option' => array(
      '#type' => 'button',
      '#name' => $okey . '_delete_option', // This needs to be unique so ajax post can work with multiple buttons.
      '#value' => '',
      '#ajax' => array(
        'callback' => 'qforms_delete_form_element_option_callback',
        'qforms_callback' => 'qforms_delete_form_element_option',
        'wrapper' => $okey, // Wrapper needs to point to element html id.
        'effect' => 'fade',
        'method' => 'replace',
        'qforms_parent' => $parent_id, // We need this reference to parent qforms element.
      ),
      '#attributes' => array('class' => array('qforms-button', 'qforms-action-delete-option'), 'title' => t('Delete option')),
    ),
  );

  return $options[$okey];
}

/**
 * Appends size field to $element.
 */
function qforms_elements_add_size(&$element_data, &$element) {
  $element[$element_data['id'] . '_data'][$element_data['id'] . '_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size'),
    '#default_value' => isset($element_data['size'])? check_plain($element_data['size']) : '0',
    '#size' => 5,
  );
}

/**
 * Generates default form element array.
 *
 * @param $data
 *   Array of element definition params.
 * @return
 *   Form array.
 */
function qforms_elements_default($data) {
  $element = array(
    '#prefix' => '<div id="' . $data['id'] . '" class="qforms-element qforms-' . $data['type'] . '">',
    '#suffix' => '</div>',
    '#weight' => $data['weight'],
  );

  $element[$data['id'] . '_header'] = array(
    '#prefix' => '<div class="qforms-element-header">',
    '#suffix' => '</div>',
  );

  $element[$data['id'] . '_header']['title'] = array(
    '#markup' => '<div class="qforms-element-title">' . $data['element_title'] . '</div>',
  );

  // Form element actions.
  $element[$data['id'] . '_header']['actions'] = array(
    '#prefix' => '<div class="qforms-element-actions">',
    '#suffix' => '</div>',
    $data['id'] . '_move' => array(
      '#markup' => '<a title="' . t('Move') . '" href="#" class="qforms-action qforms-action-move qforms-button"></a>',
    ),
    $data['id'] . '_close' => array(
      '#markup' => '<a title="' . t('Close') . '" href="#" class="qforms-action qforms-action-collapse qforms-form-close qforms-button"></a>',
    ),
    $data['id'] . '_delete' => array(
      '#type' => 'button',
      '#name' => $data['id'] . '_delete', // This needs to be unique so ajax post can work with multiple buttons.
      '#value' => '',
      '#ajax' => array(
        'callback' => 'qforms_delete_form_element_callback',
        'wrapper' => $data['id'],
        'effect' => 'fade',
        'method' => 'replace',
      ),
      '#attributes' => array('class' => array('qforms-action-delete', 'qforms-button'), 'title' => t('Delete element')),
    ),
  );

  // Data elements.
  $element[$data['id'] . '_data'] = array(
    '#prefix' => '<div class="qforms-element-data">',
    '#suffix' => '</div>',
    $data['id'] . '_title' => array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => isset($data['title'])? check_plain($data['title']) : '',
      '#required' => TRUE,
      '#size' => 30,
    ),
    $data['id'] . '_description' => array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => isset($data['description'])? check_plain($data['description']) : '',
      '#size' => 30,
    ),
    $data['id'] . '_separator' => array(
      '#markup' => '<div class="qforms-separator"></div>',
      '#weight' => 101,
    ),
    $data['id'] . '_required' => array(
      '#type' => 'checkbox',
      '#title' => t('Required'),
      '#title_display' => 'before',
      '#default_value' => isset($data['required'])? $data['required'] : 0,
      '#weight' => 102,
    ),
    $data['id'] . '_visible' => array(
      '#type' => 'checkbox',
      '#title' => t('Visible'),
      '#title_display' => 'before',
      '#default_value' => isset($data['visible'])? $data['visible'] : 1,
      '#weight' => 103,
    ),
    $data['id'] . '_weight' => array(
      '#type' => 'textfield',
      '#title' => t('Weight'),
      '#size' => 5,
      '#default_value' => isset($data['weight'])? $data['weight'] : 1,
      '#attributes' => array('class' => array('qforms-element-weight')),
      '#weight' => 104,
    ),
  );

  return $element;
}
